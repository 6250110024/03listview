import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  //const MyApp({Key? key}) : super(key: key);

  final titles = ['bike', 'bus', 'car', 'railway', 'subway', 'transit', 'walk'];
  final Image = [
    NetworkImage(
        'https://media.discordapp.net/attachments/889417067130941454/933983514108506142/2020_paddle-tomato_800.png'),
    NetworkImage
      ('https://media.discordapp.net/attachments/889417067130941454/933983764953038848/blue-bus-oi227b3c7b29fnha0rwbvkp23kgjneyy21qgyruj9c.png'),
    NetworkImage
      ('https://media.discordapp.net/attachments/889417067130941454/933984350045888582/Senna-tile-1-1200x1200crop-4x3.png?width=776&height=582'),
    NetworkImage
      ('https://cdn.discordapp.com/attachments/889417067130941454/933987297010020362/c8d58e5c-3bac-4441-a742-3262ec7f579c.png'),
    NetworkImage
      ('https://cdn.discordapp.com/attachments/889417067130941454/933988561823367168/Image3-1024x581.png'),
    NetworkImage
      ('https://cdn.discordapp.com/attachments/889417067130941454/933989130122194964/cq5dam.png'),
    NetworkImage
      ('https://cdn.discordapp.com/attachments/889417067130941454/933991331737513984/583.png'),

    //Icons.directions_bike,
    //Icons.directions_boat,
    //Icons.directions_bus,
    //Icons.directions_car,
    //Icons.directions_railway,
    //Icons.directions_run,
    //Icons.directions_subway,
    //Icons.directions_transit,
    //Icons.directions_walk
  ];

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: "Basic ListView",
      theme: ThemeData(
        primarySwatch: Colors.teal,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: Scaffold(
        appBar: AppBar(
          title: Text('ListView'),
        ),
        body: ListView.builder(
            itemCount: titles.length,
            itemBuilder: (BuildContext context, int index) {
              return Column(
                children: [
                  ListTile(
                    leading: CircleAvatar(
                      backgroundImage: (Image[index]),
                    ),
                    title: Text(
                      '${titles[index]}',
                      style: TextStyle(fontSize: 18),
                    ),
                    subtitle: Text(
                      'Microsoft',
                      style: TextStyle(
                        fontSize: 15,
                      ),
                    ),
                    trailing: Icon(
                      Icons.notifications_none,
                      size: 25,
                    ),
                    onTap: () {
                      AlertDialog alert = AlertDialog(
                        title: Text('Welcome'),
                        // To display the title it is optional
                        content: Text('This is a ${titles[index]}'),
                        // Message which will be pop up on the screen
                        // Action widget which will provide the user to acknowledge the choice
                        actions: [
                          ElevatedButton(
                            // FlatButton widget is used to make a text to work like a button
                            onPressed: () => Navigator.pop(context, 'OK'),
                            // function used to perform after pressing the button
                            child: Text('CANCEL'),
                          ),
                          ElevatedButton(
                            onPressed: () => Navigator.pop(context, 'OK'),
                            child: Text('ACCEPT'),
                          ),
                        ],
                      );
                      showDialog(
                        context: context,
                        builder: (BuildContext context) {
                          return alert;
                        },
                      );
                    },
                  ),
                  Divider(
                    thickness: 0.8,
                  ),
                ],
              );
            }),
      ),
    );
  }
}
